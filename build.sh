#!/bin/bash
set -euo pipefail
set -x
( cd database && make )
( cd reads && make )

mkdir igdiscover-testdata igdiscover-testdata/database
for i in V D J; do cp database/IGH$i.fasta igdiscover-testdata/database/$i.fasta; done

ln reads/reads.1.fastq.gz reads/reads.2.fastq.gz igdiscover-testdata/
cp README.md igdiscover-testdata/

tar czf igdiscover-testdata.tar.gz igdiscover-testdata/
rm -r igdiscover-testdata/

