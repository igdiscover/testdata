Notes on how the first dataset was created
------------------------------------------

This was done with a relatively old version of the tool and needs to be updated.

* Original dataset is "Lib1-F130"
* Run pipeline for 4 iterations with minified database
* Look at iteration-004/unique.v_usage.tab, choose good genes that have between
  500 and 1000 assigned sequences
* Run:

    igdiscover parse --barcode-length 0 iteration-004/unique.igblast.txt.gz iteration-004/unique.fasta | egrep '(VH3.2_S532128|VH4.11_S372585|VH5.7_S168962)' | cut -f37 > names.txt
    for i in 1 2; do
        ( zcat reads.$i.fastq.gz | head -n 100000 | tail -n 8000 ; zgrep -F -A 3 --no-group-separator -f names.txt reads.$i.fastq.gz ) | gzip > testdata.$i.fastq.gz
    done

Version 0.4
-----------

Reads were added to also test J discovery. The number of iterations had to be
reduced to 1 to make this work (this should be increased again).

Procedure:

* Run IgDiscover on ERR1760498 (H1 dataset)
* Pick the first 300 table rows that contain a certain J sequence ($19 is V_SHM):

    ( zcat iteration-01/filtered.tab.gz|head -n1; zcat iteration-01/filtered.tab.gz| awk 'NR==1||$19==0'|grep ATACTTCCAGCACTGGGGCCAGGGCACCCTGGTCA ) | head -n 301 > j.tab

* Run J discovery on the mini table:

    igdiscover discoverj --allele-ratio=0.1 --database=iteration-01/database/J.fasta j.tab

* Inspect the output table and make sure that it contains one row
* Create FASTA with full sequences, with barcode, forward primer and reverse complement of reverse primer:

    sed 1d j.tab | cut -f45|awk 'BEGIN{acgt[0]="A";acgt[1]="C";acgt[2]="G";acgt[3]="T"} {printf(">J_%d\nCGTGAGCTGAGTACGACTCACTATAGCTTCACAAAA%s%s%s%sAAAA%sCTTTTCCCCCTCGTCTCCTGTGAGAATGCCCCTTTTTGGCCAAAAAGGCCTGC\n", NR, acgt[int(NR/64)%4], acgt[int(NR/16)%4], acgt[int(NR/4)%4], acgt[NR%4], $1)}' > J.fasta

* Simulate the paired-end FASTQ:

    sqt fastxmod -c 33 -l 300 J.fasta > J.1.fastq
    sqt fastxmod -c 33 J.fasta -r | sqt fastxmod -l 300 - > J.2.fastq
* Append the reads in J.[12].fastq to reads.[12].fastq.gz


Version 0.5
-----------

This version is now generated with a Makefile. See database/Makefile and
reads/Makefile.


Version 0.6
-----------

Added some extra text to the FASTA headers to make sure that they parse
correctly (the extra text should be ignored).

Version 0.7
-----------

* Make one V sequence id look like a GenBank accession
* Delete first nucleotide of IGHV4-31 to force warning about region not divisible by three
