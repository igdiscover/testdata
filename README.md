IgDiscover test dataset
=======================

This repository contains the scripts for automatically creating a test
dataset for IgDiscover.

The dataset itself is not included here. Instead, download it from
<https://bitbucket.org/igdiscover/testdata/downloads/?tab=downloads>.
The test dataset is a small subset of the human Ig heavy chain dataset
ERR1760498 available on the Sequence Read Archive.  Only reads coming from a
couple of highly expressed genes were chosen to make sure that they pass the
filters for discovery.

The dataset is mainly for testing whether IgDiscover was correctly installed, so
not much is really “discovered”.

Also, since it appears that re-distributing the IMGT human IGH database may not
be legal, a database of lower quality is included that was created from Ensembl
annotations.

Instructions for how to run IgDiscover on this dataset are available at
<http://docs.igdiscover.se/en/latest/testing.html>.


How to create the dataset
-------------------------

See <https://bitbucket.org/igdiscover/testdata/> for the scripts that were
used to create the dataset.

When you have cloned that repository, run this command to create the tarball:

    ./build.sh

This will first build the database, then the set of reads, and finally create
a tarball. You will then need to rename it to include a version number and
upload it somewhere.
